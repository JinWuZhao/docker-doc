# Docker简介
## 历史和发展
`Docker`来自2013年初PaaS服务供应商`dotCloud`开源的一个内部项目。
短短几年时间迅速发展成为一个热门项目，`dotCloud`公司干脆出售了其PaaS平台业务，并改名为`Docker.Inc`，专注`Docker`的开发和推广。  

`Docker`是一个容器引擎，用来方便地管理容器，可以统一整个开发、测试和部署的环境流程，减少运维成本。
由于其轻量化的容器技术，使得`Docker`的磁盘占用、性能和效率方面相较于传统虚拟化技术都有非常明显的提高。

## 架构介绍
`Docker`架构图：  

![Docker架构图](img/docker_architecture.jpg)  

`Docker`并没有传统虚拟化中的`Hypervisor`层，省去了该层的开销，其虚拟化技术是基于操作系统内核的`Cgorup`和`Namespace`技术，处理逻辑与内核深度融合，性能接近物理机。  
`Docker`并不会直接与内核交互，它是通过一个更底层的工具Libcontainer与内核交互的。
`Libcontainer`是真正意义上的容器引擎，它通过`clone`系统调用直接创建容器，通过`pivot_root`系统调用进入容器，且通过直接操作`cgroupfs`文件实现对资源的管控，而`Docker`本身则侧重于处理更上层的业务。

# Docker的功能和组件
## Docker客户端
`Docker`是一个典型的C/S架构的应用程序，但在发布上，Docker将客户端和服务器端统一在同一个二进制文件中，不过，这只是对于Linux系统而言的，在其他平台如Mac上，`Docker`只提供了客户端。    

`Docker`客户端一般通过`Docker command`来发起请求，另外，也可以通过`Docker`提供的一整套RESTful API来发起请求，这种方式更多地被应用在应用程序的代码中。

## Docker daemon
`Docker daemon`也可以被理解成`Docker Server`，另外，人们也常常用`Docker Engine`来直接描述它，因为这实际上就是驱动整`个Docker`功能的核心引擎。    

简单地说，`Docker daemon`实现的功能就是接收客户端发来的请求，并实现请求所要求的功能，同时针对请求返回相应的结果。在功能的实现上，因为涉及了容器、镜像、存储等多方面的内容，`daemon`内部的机制会复杂很多，涉及了多个模块的实现和交互。

## Docker容器
`Docker`通过`Libcontainer`实现对容器生命周期的管理、信息的设置和查询，以及监控和通信等功能。而容器也是对镜像的完美诠释，容器以镜像为基础，同时又为镜像提供了一个标准的和隔离的执行环境。  
在概念上，容器则很好地诠释了`Docker`集装箱的理念，集装箱可以存放任何货物，可以通过邮轮将货物运输到世界各地。运输集装箱的邮轮和装载卸载集装箱的码头都不用关心集装箱里的货物，这是一种标准的集装和运输方式。
类似的，`Docker`的容器就是“软件界的集装箱”，它可以安装任意的软件和库文件，做任意的运行环境配置。开发及运维人员在转移和部署应用的时候，不用关心容器里装了什么软件，也不用了解它们是如何配置的。
而管理容器的`Docker`引擎同样不关心容器里的内容，它只要像码头工人一样让这个容器运行起来就可以了，就像所有其他容器那样。

## Docker镜像
如果说容器提供了一个完整的、隔离的运行环境，那么镜像则是这个运行环境的静态体现，是一个还没有运行起来的“运行环境”。  

相对于传统虚拟化中的ISO镜像，`Docker`镜像要轻量化很多，它只是一个可定制的`rootfs`（根文件系统）。`Docker`镜像的另一个创新是它是层级的并且是可复用的，这在实际应用场景中极为有用，多数基于相同发行版的镜像，在大多数文件的内容上都是一样的，基于此，当然会希望可以复用它们，而`Docker`做到了。
在此类应用场景中，利用`Unionfs`的特性，`Docker`会极大地减少磁盘和内存的开销。

## Registry
`Registry`是一个存放镜像的仓库，它通常被部署在互联网服务器或者云端。  

我们在公司将一个软件的运行环境制作成镜像，并上传到`Registry`中，这时就可以很方便地在家里的笔记本上，或者在客户的生产环境上直接从`Registry`上下载并运行了。  

`Docker`公司提供的官方`Registry`叫`Docker Hub`，这上面提供了大多数常用软件和发行版的官方镜像，还有无数个人用户提供的个人镜像。其实，`Registry`本身也是一个单独的开源项目，任何人都可以下载后自己部署一个`Registry`。

# 容器技术介绍
对于Linux容器的最小组成，可以由以下公式来表示：  
`容器` = `Cgroup` + `Namespace` + `rootfs` + `容器引擎（用户态工具）`

## Cgroup
`Cgroup`是`control group`的简称。属于Linux内核提供的一个特性，用于限制和隔离一组进程对系统资源的使用，也就是做资源QoS，这些资源主要包括CPU、内存、block I/O和网络带宽。`Cgroup`从2.6.24开始进入内核主线，目前各大发行版都默认打开了`Cgroup`特性。  
从实现的角度来看，`Cgroup`实现了一个通用的进程分组的框架，而不同资源的具体管理则是由各个`Cgroup`子系统实现的。截止到内核4.1版本，`Cgroup`中实现的子系统及其作用如下：  
- devices：设备权限控制。
- cpuset：分配指定的CPU和内存节点。
- cpu：控制CPU占用率。
- cpuacct：统计CPU使用情况。
- memory：限制内存的使用上限。
- freezer：冻结（暂停）Cgroup中的进程。
- net_cls：配合tc（traffic controller）限制网络带宽。
- net_prio：设置进程的网络流量优先级。
- huge_tlb：限制HugeTLB的使用。
- perf_event：允许Perf工具基于Cgroup分组做性能监测。

## Namespace
`Namespace`称为命名空间，它主要做访问隔离。Namespace是将内核的全局资源做封装，使得每个`Namespace`都有一份独立的资源，因此不同的进程在各自的`Namespace`内对同一种资源的使用不会互相干扰。 
目前Linux内核总共实现了6种`Namespace`： 
- IPC：隔离System V IPC和POSIX消息队列。
- Network：隔离网络资源。
- Mount：隔离文件系统挂载点。
- PID：隔离进程ID。
- UTS：隔离主机名和域名。
- User：隔离用户ID和组ID。

## rootfs
`rootfs`是`Root File System`的简称，主要用于文件系统隔离。

## 容器引擎
用于控制容器的生命周期的用户态工具（`Docker`）。

# Docker镜像
`Docker image`是用来启动容器的只读模板，是容器启动所需要的`rootfs`，类似于虚拟机所使用的镜像。  
下图是`Docker`镜像的表示方法，可以看到其被“/”分为了三个部分，其中每部分都可以类比`Github`中的概念。  

![Docker镜像的表示方法](img/docker_image.jpg)  

- Remote docker hub：集中存储镜像的Web服务器地址。该部分的存在使得可以区分从不同镜像库中拉取的镜像。若Docker的镜像表示中缺少该部分，说明使用的是默认镜像库，即Docker官方镜像库。
- Namespace：类似于Github中的命名空间，是一个用户或组织中所有镜像的集合。
- Repository：类似于Git仓库，一个仓库可以有多个镜像，不同镜像通过tag来区分。
- Tag：类似Git仓库中的tag，一般用来区分同一类镜像的不同版本。
- Layer：镜像由一系列层组成，每层都用64位的十六进制数表示，非常类似于Git仓库中的commit。
- Image ID：镜像最上层的layer ID就是该镜像的ID，Repo：tag提供了易于人类识别的名字，而ID便于脚本处理、操作镜像。

## 镜像的使用
### 列出本机镜像
```
$ docker images
REPOSITORY                           TAG                 IMAGE ID            CREATED             SIZE
jinwuzhao/ubuntu16_04lts_workspace   1.0                 2f831bb03512        4 weeks ago         637.3 MB
jinwuzhao/ubuntu16_04lts             latest              379733a0af67        4 weeks ago         280.4 MB
jinwuzhao/ubuntu16_04lts             1.0                 632c7d511a7a        4 weeks ago         272.6 MB
ubuntu                               latest              c73a085dc378        3 months ago        127.1 MB
```
### Build: 创建一个镜像
- 下载一个基础镜像
    ```
    $ docker pull ubuntu:latest
    ```
- 制作新的镜像，类似`git commit -m "message"`
    ```
    $ docker commit -m "message" -p [container ID | container name]
    ```
- 通过`Dockerfile`创建镜像  
    - `Dockerfile`示例
        ```bash
        # This Dockerfile uses the ubuntu image
        # VERSION 2 - EDITION 1
        # Author: tester
        # Command format: Instruction [arguments / command] ..

        # Base image to use, this must be set as the first line
        FROM ubuntu

        # Maintainer: tester  < tester at email.com> (@docker_user)
        MAINTAINER tester tester@huawei.com

        # Commands to update the image
        RUN echo "deb http://archive.ubuntu.com/ubuntu/ raring main universe" >> /etc/apt/sources.list
        RUN apt-get update && apt-get install -y nginx
        RUN echo "\ndaemon off;" >> /etc/nginx/nginx.conf
        
        # Commands when creating a new container
        CMD /usr/sbin/nginx
        ```
    - `Dockerfile`指令
        - `FROM`指令  
            格式为`FROM<image>`或`FROM<image>:<tag>`。  
            `Dockerfile`的第一条必须是`FROM`指令，用来指定要制作的镜像继承自哪个镜像。
            需要说明的是，可以在`Dockerfile`中写多个`FROM`指令来构建复杂的镜像。

        - `MAINTAINER`指令  
            格式为`MAINTAINER<name>`。  
            用来指定维护者信息。

        - `RUN`指令  
            格式为`RUN<command>`或`RUN["executable","param1","param2"...]`。  
            该指令是用来执行shell命令的，当解析`Dockerfile`时，遇到`RUN`指令，`Docker`会将该指令翻译为`/bin/sh –c "xxx"`，其中`xxx`为`RUN`后边的Shell命令。

        - `EXPOSE`指令  
            格式为`EXPOSE<port>[<port>...]`。  
            该指令用来将容器中的端口号暴露出来，也可以通过`docker run –p`命令实现和服务器端口的映射。

        - `CMD`指令  
            `CMD["executable","param1","param2"]`使用`exec`执行，推荐方式；  
            `CMD command param1 param2`在`/bin/sh`中执行，提供给需要交互的应用；  
            `CMD["param1","param2"]`提供给`ENTRYPOINT`的默认参数。  

            指定启动容器时执行的命令，每个`Dockerfile`只能有一条`CMD`指令。如果指定了多条`CMD`指令，只有最后一条会被执行。值得说明的是，如果用户启动容器时指定了运行的命令，则会覆盖掉`CMD`指定的命令。

        - `ENTRYPOINT`指令  
            该指令有两种格式：  
            `ENTRYPOINT["executable","param1","param2"]`  
            `ENTRYPOINT command param1 param2`（Shell中执行）。  

            每个`Dockerfile`中只能有一个`ENTRYPOINT`，当指定多个时，只有最后一个有效。
        
        - `VOLUME`指令  
            格式为`VOLUME["/data"]`。  
            创建一个可以从本地主机或其他容器挂载的挂载点，一般用来存放数据库或需要永久保存的数据。如果和host共享目录，`Dockerfile`中必须先创建一个挂载点，然后在启动容器的时候通过`docker run –v $HOSTPATH:$CONTAINERPATH`来挂载，其中`CONTAINERPATH`就是创建的挂载点。

        - `ENV`指令  
            格式为`ENV<key><value>`。  
            指定一个环境变量，会被后续RUN指令使用，并在容器运行时保持。

        - `ADD`指令  
            格式为`ADD<src><dest>`。  
            该指令将复制指定的`<src>`到容器中的`<dest>`。其中`<src>`可以是`Dockerfile`所在目录的一个相对路径；也可以是一个`URL`；还可以是一个`tar`文件（自动解压为目录）。

        - `COPY`指令  
            格式为`COPY<src><dest>`。  
            复制本地主机的`<src>`（为`Dockerfile`所在目录的相对路径）到容器中的`<dest>`。当使用本地目录为源目录时，推荐使用`COPY`。

    - 创建镜像  
        ```
        $ docker build <dockerfile>
        ```

### Ship: 上传镜像
- 通过Registry上传（docker push）
- 通过docker export / docker save 生成压缩包发给目标服务器
- 编写Dockerfile文件，分享给他人

### Run: 以镜像为模板启动一个容器
```
$ docker run -it -p :8888:22/tcp -h testmachine ubuntu:latest /bin/bash
```

### Docker镜像的生命周期

![Docker镜像生命周期](img/docker_image_cycle.jpg)

## 镜像的数据组成
使用以下命令查看`Docker`镜像的元数据信息：  
```
$ docker inspect [image ID]
```
部分信息关键字参考：  
- Id：Image的ID。可以看到image ID实际上只是最上层的layer ID，所以docker inspect也适用于任意一层layer。
- Parent：该layer的父层，可以递归地获得某个image的所有layer信息。
- Comment：非常类似于Git的commit message，可以为该层做一些历史记录，方便其他人理解。
- Container：这个条目比较有意思，其中包含哲学的味道。比如前面提到容器的启动需要以image为模板。但又可以把该容器保存为镜像，所以一般来说image的每个layer都保存自一个容器，所以该容器可以说是image layer的“模板”。
- Config：包含了该image的一些配置信息，其中比较重要的是：“env”容器启动时会作为容器的环境变量；“Cmd”作为容器启动时的默认命令；“Labels”参数可以用于docker images命令过滤。
- Architecture：该image对应的CPU体系结构。现在Docker官方支持amd64，对其他体系架构的支持也在进行中。

## 镜像的分层原理
### 联合挂载（Union mounts）
联合文件系统会把多个目录（可能对应不同的文件系统）挂载到同一个目录，对外呈现这些目录的联合。  

### 写时复制（Copy on write）
`LiveCD`领域里，首先将CD只读挂载到特定目录，然后在之上覆盖一层可读可写的文件层。任何导致文件变动的修改都会被添加到新的文件层内。

# Docker网络
## Libnetwork
`Docker`官方的网络容器模型。  

![Libnetwork CNM](img/docker_cnm.jpg)  

实现的五种驱动：  
- bridge：Docker默认的容器网络驱动。Container通过一对veth pair连接到docker0网桥上，由Docker为容器动态分配IP及配置路由、防火墙规则等。
- host：容器与主机共享同一Network Namespace，共享同一套网络协议栈、路由表及iptables规则等。容器与主机看到的是相同的网络视图。
- null：容器内网络配置为空，需要用户手动为容器配置网络接口及路由等。
- remote：Docker网络插件的实现。Remote driver使得Libnetwork可以通过HTTP RESTful API对接第三方的网络方案，类似SocketPlane的SDN方案只要实现了约定的HTTP URL处理函数及底层的网络接口配置方法，就可以替换Docker原生的网络实现。
- overlay：Docker原生的跨主机多子网网络方案。主要通过使用Linux bridge和vxlan隧道实现，底层通过类似于etcd或consul的KV存储系统实现多机的信息同步。over-lay驱动当前还未正式发布，但开发者可以通过编译实验版的Docker来尝试使用，Docker实验版同时提供了额外的network和service子命令来进行更灵活的网络操作，不过，需要内核版本>=3.16才可正常使用。

bridge网络模型：  

![bridge网络模型](img/docker_bridge.jpg)

## 网络相关配置

- 映射容器的端口到主机上
    ```
    $ docker run -dt -p :8888:22/tcp -p :8080:80/tcp -p :7443:443/tcp -p :3306:3306/tcp -h [hostname] -name [container name] [image ID]
    ```
    以上命令会根据`image ID`启动一个容器，`-h`可以设置容器在网络中的主机名称，`-p`用来配置端口映射。  
    这个例子里把容器的`22`端口映射到了`Docker`宿主机的`8888`端口，这样可以通过`ssh root@localhost -p 8888`在宿主机上通过ssh访问容器了。  
    此外还映射了`80`到`8888`、`443`到`7443`、`3306`到`3306`。  

- 为容器间建立连接
    ```
    $ docker run -tid --name=c1 ubuntu:latest bash
    $ docker run -tid --link=c1:alias_c1 --name=c2
    ```
    以上首先命令创建了一个名为`c1`的容器，然后创建了一个名为`c2`的容器，并链接到`c1`容器并取别名为`alias_c1`。于是`c2`便可以通过网络直接访问到`c1`。  


# 容器卷
`Docker`容器里产生的数据，如果不通过`docker commit`生成新的镜像，使数据作为镜像的一部分保存下来，就会在容器删除后丢失。
为了能够持久化保存和共享容器的数据，`Docker`提出了`卷`（volume）的概念。
简单来讲，`卷`就是目录或文件，由`Docker daemon`挂载到容器中，因此不属于联合文件系统，卷中的数据在容器被删除后仍然可以访问。  
`Docker`提供了两种管理数据的方式：`数据卷`和`数据卷容器`。

## 创建数据卷
用户可以在执行`docker create`或`docker run`命令时使用`-v`参数来添加数据卷，也可以通过多次指定该参数来挂载多个数据卷。
```
$ docker run -d -v /host/data:/data --name [container name] [image name]
```
上述命令可以将`Docker daemon`所在主机的`/host/data`目录挂载到容器的`/data`路径下。  
将主机目录挂载为数据卷的功能在有些场景下非常有用。比如，我们把程序的编译环境打包在一个`Docker`镜像里，当需要编译修改过的源码时，只需要将源码目录作为数据卷挂载到`Docker`容器里即可。

还可以以只读的方式挂载一个数据卷，如下：
```
$ docker run -ti -v /host/data:/data:ro --name [container name] [image name]
```

## 创建数据卷容器
如果用户需要在容器之间共享一些需要永久存储的数据，或者想要使用一个临时容器中的相关数据，可以创建一个数据卷容器，然后使用该容器进行数据共享。  

例如想要创建多个`Postgres`数据库，并且希望这些数据库之间共享数据，可以先创建一个数据卷容器，该容器中并不运行任何应用：
```
$ docker create -v /dbdata --name dbdata training/postgres /bin/true
```
然后启动`Postgres`数据库服务，使用`--volumes-from`参数将上面生成的数据卷挂载进来，之后启动多个容器，各个容器之间就可以通过`dbdata`数据卷共享数据了：
```
$ docker run -d --volumes-from dbdata --name db1 training/postgres
$ docker run -d --volumes-from dbdata --name db2 training/postgres
```
也可以使用`--volume-from db1`或`--volume-from db2`的方式挂载`dbdata`数据卷：
```
$ docker run -d --name db3 --volumes-from db1 training/postgres
```
使用数据卷容器存储的数据不会轻易丢失，即便删除`db1`、`db2`容器甚至是初始化该数据卷的`dbdata`容器，该数据卷也不会被删除。只有在删除最后一个使用该数据卷的容器时显式地指定`docker rm –v $CONTAINER`才会删除该数据卷。

## 数据卷的备份、转储和迁移
使用数据卷的方式管理容器数据时，可以很方便地对其中的数据进行备份、转储和迁移。  
可以使用如下命令将数据卷中的数据打包，并将打包后的文件拷贝到主机当前目录中：
```
$ docker run –-rm --volumes-from dbdata -v $(pwd):/backup ubuntu tar cvf /backup/backup.tar /dbdata
```
上述命令创建了一个容器，该容器挂载了`dbdata`数据卷，并将主机的当前目录挂载到了容器的`/backup`目录中；然后在容器中使用`tar`命令将`dbdata`数据卷中的内容打包存放到`/backup`目录的`backup.tar`文件中。  
待容器执行结束后，备份文件就会出现在主机的当前目录。之后可以将该备份文件恢复到当前容器或新创建的容器中，完成数据的备份和迁移工作。

# Docker应用
## 基于`Docker`容器发布应用
## 将`Docker`打造成为工作空间